# Front end for Telia project

## The front end will be built in React and styled using Material UI

### The front end will provide an interface for URI endpoints defined in the backend API

---

## Redux Thingy

Evrytime we need to change our state, for example log in user (=== update state), we are dispatching the function, which is going to do that.

For example, everytime somebody goes into our website, we need to check if that user is logged in or not. So, in our App component we put this code

```
	const dispatch = useDispatch()

    useEffect(() => {
        dispatch(initializeUserState())
    }, [])
```

##### \*With useEffect hook dispatch operation will be called only one time.

Let's take a look into initializeUserState() function

```
export const initializeUserState = () => {
    return async (dispatch) => {
        let inState = await axios
            .post(userRetrieveUrl)
            .then((resp) => resp.data)
        dispatch({
            type: actionTypes.SET_USER,
            payload: inState ? inState : null,
        })
    }
}
```

Since we are using redux-thunk, we could dispatch functions, and that's why in our App component we have dispatched the function, which is going to fetch from our BE the user object and only after that it is going to dispatch the actual user object to our reducer

Let us go there.

---

```
import * as actionTypes from './userActions'

export const userReducer = (state = null, action) => {
    switch (action.type) {
        case actionTypes.SET_USER:
            return action.payload
        default:
            return state
    }
}

```

This is our user Reducer. This function is going to update our state according to the action type dispatched with our user object

---

All of this is really great, but how are we going to extract the user object from our state?

Really easy

```
const user = useSelector((state) => state.user)
```

We just using useSelector hook to find out anything from our big state

---

Also, very important moment, about store is combining reducers, since we are going to have a lot of data there. For every type of data we have different reducers and then we are just combinig them using combineReducers function.

Into that function we are passing object, in which to every key(name) there is a value(reducer)

This is needed for us to extract things from state. Do you remember, when we were trying to select our user from state we used this form

```
state => state.user
```

This way, every key in combineReducers object that is standing in front of the actual reducer is needed to extract data from state.

```
import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import { userReducer } from './user/userReducer'

const rootReducer = combineReducers({ user: userReducer })

export const store = createStore(rootReducer, applyMiddleware(thunk))

```
