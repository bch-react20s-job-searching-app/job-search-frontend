import React from 'react'
import styled from 'styled-components'

const LargeBtn = styled.button `
    background: ${({theme}) => theme.color.bg};
    color: ${({theme}) => theme.color.accent2.main};
    width: 12.5rem;
    height: 3.75rem;
    font-size: ${({theme}) => theme.font.size.largeButton};
    padding: .5rem 2rem;
    font-weight: bold;
    border-radius: 40px;
    font-family: ${({theme}) => theme.font.style.text};
    box-shadow: -6px -6px 6px ${({theme}) => theme.color.lightShadow}, 6px 6px 6px ${({theme}) => theme.color.darkShadow};
    border: .5px solid ${({theme}) => theme.color.darkShadow};

    &:hover {
        background: ${({theme}) => theme.color.accent2.main};
        color: ${({theme}) => theme.color.bg};
        transition: 300ms;
    }
`

export default function LargeButton({ name, onClick }) {
    return (
        <LargeBtn id="large-button" onClick={onClick}>
            {name}
        </LargeBtn>
    )
}
