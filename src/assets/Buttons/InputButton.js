import React from 'react'
import styled from 'styled-components'

const InputBtn = styled.button `
    background: ${({theme}) => theme.color.bg};
    color: ${({theme}) => theme.color.accent1.main}; 
    width: auto;  
    min-width: 150px;    
    height: 2.5rem;
    font-size: ${({theme}) => theme.font.size.smallButton};
    font-weight: bold;
    border-radius: 40px;
    font-family: ${({theme}) => theme.font.style.text};
    box-shadow: -6px -6px 6px ${({theme}) => theme.color.lightShadow}, 6px 6px 6px ${({theme}) => theme.color.darkShadow};
    border: .5px solid ${({theme}) => theme.color.darkShadow};

    &:hover {
        background: ${({theme}) => theme.color.accent1.main};
        color: ${({theme}) => theme.color.bg};
        transition: 300ms;
    }
`

export default function InputButton({type, name, onClick}) {
  
    return (
        <InputBtn className='m-3 align-self-center' type={type} onClick={onClick}>{name}</InputBtn>
    );
  }