import React from 'react'
import './InputField.css'
import { MDBInput } from 'mdb-react-ui-kit'

export default function InputField({type}) {
  
    return (
        <MDBInput 
        className="input-field"
        type={type}
        
        />
    );
  }