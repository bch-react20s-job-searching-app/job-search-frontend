import { MDBCol } from 'mdb-react-ui-kit'
import React from 'react'
import styled from 'styled-components'

const StyledTextarea = styled.textarea`
    background: ${({ theme }) => theme.color.bg};
    width: 100%;
    min-height: 100px;
    border-radius: 20px;
    font-family: ${({ theme }) => theme.font.style.text};
    box-shadow: inset -6px -6px 6px ${({ theme }) => theme.color.lightShadow},
        inset 6px 6px 6px ${({ theme }) => theme.color.darkShadow};
    border: 0.5px solid ${({ theme }) => theme.color.darkShadow};
    padding: 20px;
    resize: vertical;
    overflow: auto;
    outline: none;

    &.err {
        border: 1px solid #e31c8f;
    }
    @media (max-width: 576px) {
        width: 95%;
    }
`

export default function TextArea({
    name,
    id,
    onChange,
    value = '',
    placeholder,
    err = false,
    labelMsg = false,
}) {
    return (
        <MDBCol>
            <label className={'formLabel'} htmlFor={id}>
                {labelMsg ? labelMsg : name}
            </label>
            <StyledTextarea
                type="text"
                name={name}
                id={id}
                value={value || undefined}
                placeholder={placeholder}
                onChange={onChange}
                className={`input-field ${err && 'err'}`}
            />
        </MDBCol>
    )
}
