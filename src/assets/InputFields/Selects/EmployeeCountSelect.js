import React, { useState } from "react";
import Select from "react-select";

export default function EmployeeCountSelect (props) {
  
  const [employeeCount, setEmployeeCount] = useState('');

  const onChange = selectedOption => {
    setEmployeeCount(selectedOption);
  };

  return (
    <>
    <label htmlFor='employeesCount'>Employee count</label>
    <Select options={props.options} onChange={onChange} value={employeeCount} />
    </>
  )
};

