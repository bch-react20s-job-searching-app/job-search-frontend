import React, { useState } from 'react'
import Select from 'react-select'

export default function SenioritySelect({ options }) {
    const [seniority, setSeniority] = useState('')

    const onChange = (selectedOption) => {
        setSeniority(selectedOption)
    }

    return (
        <>
            <label htmlFor="seniority">Seniority</label>
            <Select options={options} onChange={onChange} value={seniority} />
        </>
    )
}
