import React, { useState } from 'react'
import Select from 'react-select'

export default function SkillsSelect({ options }) {
    const [skills, setSkills] = useState('')

    const onChange = (selectedOption) => {
        setSkills(selectedOption)
    }

    return (
        <>
            <label htmlFor="skills">Skills</label>
            <Select
                options={options}
                onChange={onChange}
                value={skills}
                isMulti
            />
        </>
    )
}
