import React, { useState } from "react";
import Select from "react-select";

export default function RemoteSelect (props) {
  
  const [remote, setRemote] = useState('');

  const onChange = selectedOption => {
    setRemote(selectedOption);
  };

  return (
    <>
    <label htmlFor='possibilityRemote'>Possibility for remote</label>
    <Select options={props.options} onChange={onChange} value={remote}/>
    </>
  )
};
