import React, { useState } from "react";
import Select from "react-select";

export default function ExperienceSelect (props) {
  
  const [experience, setExperience] = useState('');

  const onChange = selectedOption => {
    setExperience(selectedOption);
  };

  return (
    <>
    <label htmlFor='experience'>Experience</label>
    <Select options={props.options} onChange={onChange} value={experience}/>
    </>
  )
};
