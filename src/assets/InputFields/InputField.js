import React from 'react'
import styled from 'styled-components'

const InputFl = styled.input`
    background: ${({ theme }) => theme.color.bg};
    width: 100%;
    height: auto;
    border-radius: 2.5rem;
    font-family: ${({ theme }) => theme.font.style.text};
    font-size: ${({ theme }) => theme.font.size.smallButton};
    font-weight: bold;
    color: ${({ theme }) => theme.color.text};
    padding: 0.5rem 1rem;
    margin: 0 0 0.5rem 0;
    box-shadow: inset -6px -6px 6px ${({ theme }) => theme.color.lightShadow},
        inset 6px 6px 6px ${({ theme }) => theme.color.darkShadow};
    border: 0.5px solid ${({ theme }) => theme.color.darkShadow};
    outline: none;

    &[type='radio'] {
        width: 10%;
        box-shadow: none;
    }
    &.err {
        border: 1px solid #e31c8f;
    }
    @media (max-width: 400px) {
        border: 1px solid ${({ theme }) => theme.color.darkShadow};
    }
`
export default function InputField({
    name,
    id,
    type,
    onChange,
    value = '',
    err = false,
    labelMsg = false,
}) {
    return (
        <>
            <label className={'formLabel'} htmlFor={id}>
                {
                    //This commented line is the previous version of this input, it is not needed anymore, but in case of emergency, maybe this could cause the problem
                    /* {type === 'radio' ? labelMsg : name} */
                }
                {labelMsg ? labelMsg : name}
            </label>
            <InputFl
                name={name}
                id={id}
                type={type}
                value={value || undefined}
                onChange={(e) => onChange(e)}
                className={`input-field ${err && 'err'}`}
            />
        </>
    )
}
