import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { App } from './App'
import './index.css'
import { store } from './store/store'
import GlobalStyling from './theme/GlobalStyles'
import Theme from '../src/theme/Theme'

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Theme>
                <GlobalStyling />            
                <App />
            </Theme>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
)
