// For forms, dropdown select items

// Seniority
const seniority = [
  'Senior',
  'Mid',
  'Junior',
  'Intern'
]

export const seniorityOptions =  seniority.map((item) => {
  return {
    value: item,
    label: item
  }
})

// Experience
const experience = [
  '<1 year',
  '1-2 years',
  '3-5 years',
  '6-10 years',
  '>10 years'
];

export const experienceOptions = experience.map((item) => {
  return {
    value: item,
    label: item,
  };
});

// Skills
const skills = [
  'HTML',
  'CSS',
  'React',
  'Node',
  'Express',
  'Javascript',
  'Typescript', 
  'Java',
  'C#',
  'C++',
  'Python',
  'Ruby',
  'Rust',
  'Deployment',
  'Cloud',
  'MongoDB',
  'MySQL',
  'MariaDB'
];

export const skillsOptions = skills.map((item) => {
  return {
    value: item,
    label: item,
  };
});

// Remote
const remote = [
  'Yes',
  'No',
  'Partial'
];

export const remoteOptions = remote.map((item) => {
  return {
    value: item,
    label: item,
  };
});

// Employee count
const employeeCount = [
  '1-5',
  '6-10',
  '11-25',
  '26-50',
  '51-100',
  '>100'
];

export const employeeCountOptions = employeeCount.map((item) => {
  return {
    value: item,
    label: item,
  };
});
