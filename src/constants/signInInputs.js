const signInInputs = [
    { name: 'email', id: 'emailInput', type: 'email' },
    { name: 'password', id: 'passwordInput', type: 'password' },
]

export default signInInputs
