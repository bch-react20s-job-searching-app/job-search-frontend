const signUpErrors = [
    { type: 'emailInput', message: 'You have provided invalid email' },
    {
        type: 'passwordInput',
        message: 'Password has to be at least 8 characters',
    },
    {
        type: 'usernameInput',
        message: 'Please choose a username',
    },
    {
        type: 'type',
        message:
            'Looking for work? Hiring new talent? Please select  your profile type',
    },
]

export default signUpErrors
