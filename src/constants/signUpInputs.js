const signUpInputs = [
    {
        name: 'type',
        id: 'employer',
        type: 'radio',
        value: 'employer',
        labelMsg: 'I represent a company',
    },
    {
        name: 'type',
        id: 'employee',
        type: 'radio',
        value: 'employee',
        labelMsg: 'I am a jobseeker',
    },
    { name: 'email', id: 'emailInput', type: 'email' },
    { name: 'username', id: 'usernameInput', type: 'text' },
    { name: 'password', id: 'passwordInput', type: 'password' },
]

export default signUpInputs
