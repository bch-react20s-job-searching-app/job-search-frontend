const signInErrors = [
    { type: 'emailInput', message: 'Invalid email format' },
    { type: 'passwordInput', message: 'Password too short' },
]

export default signInErrors
