const createJobInputs = [
    { name: 'title', labelMsg: 'Title', id: 'title', type: 'text' },
    {
        name: 'seniority',
        labelMsg: 'Seniority',
        id: 'seniority',
        type: 'text',
    },
    {
        name: 'description',
        labelMsg: 'Description',
        id: 'description',
        type: 'text',
    },
    {
        name: 'requiredSkills',
        labelMsg: 'Required Skills, split them with comma(,)',
        id: 'requiredSkills',
        type: 'text',
    },
    {
        name: 'desirableSkills',
        labelMsg: 'Desirable Skills, split them with comma(,)',
        id: 'desirableSkills',
        type: 'text',
    },
    {
        name: 'location',
        labelMsg: 'Location, split them with comma(,)',
        id: 'location',
        type: 'text',
    },
    {
        name: 'possibilityRemote',
        labelMsg: 'Possibility Remote',
        id: 'possibilityRemote',
        type: 'checkbox',
    },
    {
        name: 'photo',
        labelMsg: 'Image',
        id: 'photo',
        type: 'file',
    },
    {
        name: 'workingLanguage',
        labelMsg: 'Working Language',
        id: 'workingLanguage',
        type: 'text',
    },
    {
        name: 'companyWebsite',
        labelMsg: 'Company Website',
        id: 'companyWebsite',
        type: 'text',
    },
    {
        name: 'contractType',
        labelMsg: 'Contract Type, split them with comma(,)',
        id: 'contractType',
        type: 'text',
    },
]

export default createJobInputs
