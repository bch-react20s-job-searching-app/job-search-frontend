const createJobErrors = [
    { type: 'title', message: 'You have forget to add a title' },
    { type: 'seniority', message: 'You have forget to add a seniority' },
    {
        type: 'description',
        message: 'You have forget to add a description',
    },
    {
        type: 'requiredSkills',
        message: 'You have forget to add a required skills',
    },
    {
        type: 'desirableSkills',
        message: 'You have forget to add a desirable skills',
    },
    { type: 'location', message: 'You have forget to add a location' },
    {
        type: 'possibilityRemote',
        message: 'You have forget to check the possibility of remote work',
    },
    { type: 'photo', message: 'You have forget to add a image' },
    {
        type: 'workingLanguage',
        message: 'You have forget to add a working language',
    },
    {
        type: 'companyWebsite',
        message: 'You have forget to add a company website',
    },
    {
        type: 'contractType',
        message: 'You have forget to add a contract type',
    },
]

export default createJobErrors
