import * as actionTypes from './userActions'

export const userReducer = (state = null, action) => {
    switch (action.type) {
        case actionTypes.SET_USER:
            return action.payload
        default:
            return state
    }
}


