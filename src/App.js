import { MDBContainer } from 'mdb-react-ui-kit'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
    BrowserRouter as Router,
    Redirect,
    Route,
    Switch,
} from 'react-router-dom'
import Chat from './components/Chat/Chat'
import Footer from './components/Footer/Footer'
import Header from './components/Header/Header'
import { GoogleDecision } from './components/Landing/Auth/SignIn/GoogleDecision/GoogleDecision'
import SignIn from './components/Landing/Auth/SignIn/SignIn'
import { SignUp } from './components/Landing/Auth/SignUp/SignUp'
import Landing from './components/Landing/Landing'
import CompanyMain from './components/Main/CompanyMain'
import { CreateJob } from './components/Main/Jobs/CreateJob'
import JobseekerMain from './components/Main/JobseekerMain'
import CompanyProfile from './components/Main/Users/CompanyProfile/CompanyProfile'
import EditJobseekerProfile from './components/Main/Users/JobseekerProfile/EditJobseekerProfile'
import JobseekerProfile from './components/Main/Users/JobseekerProfile/JobseekerProfile'
import Match from './components/Match/Match'
import { initializeUserState } from './services/userDispatcher'

export const App = () => {
    const dispatch = useDispatch()

    //Fetching user from global state
    const user = useSelector((state) => state.user)

    useEffect(() => {
        //Dispatching function to initialize the user state
        //Another words, checking if user is logged in, or not
        dispatch(initializeUserState())
    }, [dispatch])

    return user ? (
        user.unsaved ? (
            //Here we checking for the situation, when user has signed up with Google
            <GoogleDecision />
        ) : (
            <div>
                <MDBContainer fluid>
                    <Router>
                        <Header />
                        <Switch>
                            <Route exact path={'/'}>
                                {
                                    //Checking if user has company of jobSeeker account
                                    user.yTunnus ? (
                                        <CompanyMain />
                                    ) : (
                                        <JobseekerMain />
                                    )
                                }
                            </Route>
                            <Route exact path={'/profile'}>
                                {
                                    //Checking if user has company of jobSeeker account
                                    user.yTunnus ? (
                                        <CompanyProfile />
                                    ) : (
                                        <JobseekerProfile />
                                    )
                                }
                            </Route>
                            <Route
                                path={'/profile/edit'}
                                exact
                                component={EditJobseekerProfile}
                            ></Route>
                            <Route path={'/job'}>
                                {
                                    //Checking if user has company of jobSeeker account
                                    user.yTunnus ? (
                                        <CreateJob />
                                    ) : (
                                        <Redirect to={'/'} />
                                    )
                                }
                            </Route>
                            <Route path="/messages" exact component={Chat} />
                            <Route
                                path="/matchmaking"
                                exact
                                component={Match}
                            />
                        </Switch>
                        <Footer />
                    </Router>
                </MDBContainer>
            </div>
        )
    ) : (
        <div>
            <MDBContainer fluid>
                <Router>
                    <Header />
                    <Switch>
                        <Route path="/" exact component={Landing} />
                        <Route path="/signup" exact component={SignUp} />
                        <Route path="/signin" exact component={SignIn} />
                        <Route path="/*">
                            <Redirect to={'/'} />
                        </Route>
                    </Switch>
                    <Footer />
                </Router>
            </MDBContainer>
        </div>
    )
}
