import React from 'react';
import styled from 'styled-components'

const BurgerIcon = styled.i `
  position: absolute;
  background: transparent;
  z-index: 1;
`
export default function Burger({ open, setOpen }) {

  return (
    <div 
      className='pt-4 d-flex justify-content-end' 
      open={open} 
      onClick={() => setOpen(!open)}> 
      <BurgerIcon 
        className='fas fa-hamburger' 
        id='burger'         
      />  
    </div>
  )
}