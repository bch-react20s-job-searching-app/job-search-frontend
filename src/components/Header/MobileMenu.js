import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'
import { signOutUser } from '../../services/userDispatcher'

const StyledMenu = styled.nav`
    display: flex;
    flex-direction: column;
    text-align: right;
    background: ${({ theme }) => theme.color.lightShadow};
    border-radius: 20px;
    transform: ${({ open }) => (open ? 'translateX(90%)' : 'translateX(300%)')};
    height: auto;
    width: 50vw;
    padding: 1rem;
    margin-left: 0.5rem;
    position: absolute;
    top: 0;
    left: 0;
    transition: transform 0.3s ease-in-out;
    z-index: 10;
    a {
        margin: 1rem 0;
        transition: color 200ms linear;
        &:hover {
            color: #343078;
        }
    }
`

export default function Menu({ open }) {

    const dispatch = useDispatch()
    const user = useSelector((state) => state.user)

    if (user) {
        return (
            <StyledMenu open={open}>
                <Link to="/about"><i className="far fa-question-circle"></i>About</Link>
                <Link to="/privacy"><i className="far fa-user"></i>Privacy</Link>
                <Link to="/contact"><i className="fas fa-at"></i>Contact us</Link>
                <Link to="/"><i className="fas fa-home"></i>Home</Link>
                <Link to="" onClick={(e) => {
                        e.preventDefault()
                        dispatch(signOutUser())
                    }}><i className="fas fa-sign-out-alt"></i>Log out</Link>
            </StyledMenu>
        )
    } else {
        return (
            <StyledMenu open={open}>
                <Link to="/about"><i className="far fa-question-circle"></i>About</Link>
                <Link to="/privacy"><i className="far fa-user"></i>Privacy</Link>
                <Link to="/contact"><i className="fas fa-at"></i>Contact us</Link>
                <Link to="/"><i className="fas fa-home" ></i>Home</Link>
            </StyledMenu>
        )
    }

}
