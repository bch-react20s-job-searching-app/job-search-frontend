import React, { useState, useRef } from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit';
import  { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import Burger from './Burger';
import MobileMenu from './MobileMenu'
import logo from '../../assets/images/logo.svg'
import { ClickOutside } from '../../hooks'
import { signOutUser } from '../../services/userDispatcher'
import styled from 'styled-components'

const StyledHeader = styled.div `
    background: ${({ theme }) => theme.color.bg};
`

export default function Header() {

  const[open, setOpen] = useState(false)
  const currElement = useRef()
  ClickOutside(currElement, () => setOpen(false))   
  const dispatch = useDispatch()

  // For refreshing the component
  
  // eslint-disable-next-line
  const [value, setValue] = useState()  
  const reload = () => {
    setValue({})
  }

  // For rendering different content based on path & is user signed in
  const Headerpath = () => {
    const path = window.location.pathname;
    const user = useSelector((state) => state.user)

    if (!user && path === '/') {
      return (
        <div className='d-flex justify-content-end'>
          <p className='pe-5'>Already have an account?</p>
          <Link to="/signin" onClick={reload}><span>Sign in</span></Link> 
        </div> 
      )
    }
    if (!user && path === '/signin') {
      return (
        <div className='d-flex justify-content-end'>
          <p className='pe-5'>Don't have an account yet?</p>
          <Link to="/signup" onClick={reload}><span>Sign up</span></Link> 
        </div> 
      )
    }
    if (!user && path === '/signup') {
      return (
        <div className='d-flex justify-content-end'>
          <p className='pe-5'>Already have an account?</p>
          <Link to="/signin" onClick={reload}><span>Sign in</span></Link> 
        </div> 
      )
    }
    else {
      return (        
        <div className='d-flex justify-content-end'> 
         {/* <a> needed here for consistent styling only      */}
         {/* eslint-disable-next-line */}
          <a href=''><span          
          onClick={(e) => {
            e.preventDefault()
            dispatch(signOutUser())
            reload()
          }}>Log out</span></a>
        </div>  
      )
    } 
  }

    return (
      <StyledHeader>
          <MDBContainer fluid className='mt-4 mb-5'>
              <MDBRow >
                <MDBCol className='d-none d-md-block'>              
                  <Link to='/'><img src={logo} alt='logo' onClick={reload}></img></Link>                                           
                </MDBCol>
                <MDBCol className='d-none d-md-block'>
                  <Headerpath/> 
                </MDBCol>
                <MDBCol ref={currElement} className='pe-4 d-md-none'>
                  <Burger open={open} setOpen={setOpen}/>
                  <MobileMenu open={open}/>
                </MDBCol>
              </MDBRow>
          </MDBContainer>
      </StyledHeader>
  );
}