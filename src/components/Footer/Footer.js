import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React from 'react'
import styled from 'styled-components'

const FooterComponent = styled.div`
    box-shadow: inset 0 0 10px ${({ theme }) => theme.color.darkShadow};
    height: 6.25rem;
    width: 100vw;
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;

    @media (max-width: 576px) {
        display: none;
    }
`

export default function Footer() {
   
    return (
        <FooterComponent>
            <MDBContainer className="p-4">
                <MDBRow>
                    <MDBCol sm="4" className="my-1 text-center">
                        <a href="/about" className="ms-auto">
                            <span>About</span>
                        </a>
                    </MDBCol>
                    <MDBCol sm="4" className="my-1 text-center">
                        <a href="/privacy" className="ms-auto">
                            <span>Privacy</span>
                        </a>
                    </MDBCol>
                    <MDBCol sm="4" className="my-1 text-center">
                        <a href="/contact" className="ms-auto">
                            <span>Contact</span>
                        </a>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </FooterComponent>
    )
}
