import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React from 'react'
import { Link } from 'react-router-dom'
import InputButton from '../../assets/Buttons/InputButton'
import { ReactComponent as TeamLight } from '../../assets/images/TeamLight.svg'
import LandingModal from './LandingModal/LandingModal'

export default function Landing() {
    return (
        <MDBContainer className="p-1 text-center">
            <MDBRow className="d-flex-column justify-content-center">
                <MDBCol md="12">
                    <h1>Silta</h1>
                </MDBCol>
                <MDBCol md="12">
                    <h2>Connecting jobseekers and companies</h2>
                </MDBCol>
            </MDBRow>
            <MDBRow className="align-items-center">
                <MDBCol lg="6" md="12">
                    <div className="m-3 m-md-5">
                        <h2>Employee</h2>
                        <h3 className="my-2">
                            Are you a future star or a seasoned pro on a quest
                            to find the perfect job?
                        </h3>
                    </div>
                    <div className="p-3 p-md-5">
                        <h2>Employer</h2>
                        <h3 className="my-2">
                            Looking for a talent to to fill in the gap in your
                            company?
                        </h3>
                    </div>
                    <Link to="/signup">
                        <InputButton name="Sign up" />
                    </Link>
                </MDBCol>
                <MDBCol lg="6" md="12" className="p-3 mb-5 d-none d-md-block">
                    <TeamLight className='mb-3' style={{width: '100%'}}/>
                    <LandingModal />
                </MDBCol>
                <MDBCol lg="6" md="12" className="p-3 d-md-none">
                    <Link to='/signin'><p  className='mb-0'>Already have an account?</p><span>Sign in</span></Link>
                </MDBCol>
                <MDBCol lg="6" md="12" className="p-3 d-md-none">
                    <LandingModal/>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    )
}
