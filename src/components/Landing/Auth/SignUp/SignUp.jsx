import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router'
import styled from 'styled-components'
import InputButton from '../../../../assets/Buttons/InputButton'
import { ReactComponent as ComputerLight } from '../../../../assets/images/ComputerLight.svg'
import InputField from '../../../../assets/InputFields/InputField'
import signUpErrors from '../../../../constants/signUpErrors'
import signUpInputs from '../../../../constants/signUpInputs'
import { registerUser } from '../../../../services/userDispatcher'
import { signUp } from '../../../../services/validator'

const SignUpForm = styled.form`
    display: flex;
    width: auto;
    max-width: 300px;
    flex-direction: column;
    justify-content: center;
    background: ${({ theme }) => theme.color.bg};
    padding: 2rem;
    margin: 0 auto;
`

export const SignUp = () => {
    const dispatch = useDispatch()
    const history = useHistory()

    const [inputs] = useState(signUpInputs)

    //Error messages
    const [errorsMsg] = useState(signUpErrors)

    //Initializing form state object
    const [form, setForm] = useState({})

    //Initializing error state array
    const [err, setErr] = useState([])

    //Initializing info message
    const [msg, setMsg] = useState('')

    //ChangeHandler for inputs
    const changeHandler = (e) => {
        //special handler for radio inputs
        if (e.target.type === 'radio') {
            setForm({ ...form, type: e.target.value })
            return
        }

        setForm({ ...form, [e.target.name]: e.target.value })
    }

    /**
     * Launches validation and if there are not problems dispatches form into userDispatcher
     */
    const submitHandler = (e) => {
        e.preventDefault()

        //refreshing error state
        setErr([])

        //Checking form again
        const validationResult = signUp(form)
        if (validationResult.length > 0) {
            setErr(validationResult)
            return
        }

        //If everything ok -> dispatching form to the dispatcher
        dispatch(registerUser(form)).then((r) => {
            //After success authentication redirects you to the root route
            r !== 'authenticated' ? setMsg(r) : history.push('/')
        })
    }

    return (
        <MDBContainer fluid className="d-flex justify-content-center py-5">
            <MDBRow>
                <MDBCol className="d-flex-column">
                    <h3 className="text-center px-5">
                        Sign up and create yourself or to your company a profile
                    </h3>
                    <SignUpForm>
                        {inputs.map((input) => (
                            <InputField
                                labelMsg={input.labelMsg || false}
                                key={input.id}
                                name={input.name}
                                id={input.id}
                                type={input.type}
                                value={input.value}
                                onChange={(e) => {
                                    changeHandler(e)
                                }}
                                err={
                                    err.includes(input.id) ||
                                    //Special err validation for radio input
                                    (input.type === 'radio' &&
                                        err.includes('type'))
                                }
                            />
                        ))}
                        <p
                            style={{
                                color: 'red',
                                padding: '0',
                                fontSize: '12px',
                                marginBottom: '0.1rem',
                            }}
                            className="d-flex flex-row justify-content-center"
                        >
                            {err.length > 0 &&
                                errorsMsg.filter((e) => e.type === err[0])[0]
                                    .message}
                            {msg}
                        </p>
                        <InputButton name={'Sign up'} onClick={submitHandler} />
                    </SignUpForm>
                </MDBCol>
                <MDBCol sm="12" lg="6">
                    <ComputerLight className="w-100 p-5 d-none d-sm-block align-self-center computerLight" />
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    )
}
