import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import InputButton from '../../../../../assets/Buttons/InputButton'
import LargeButton from '../../../../../assets/Buttons/LargeButton'
import {
    googleAuth,
    registerUser,
    signOutUser,
} from '../../../../../services/userDispatcher'

/**
 * This component is shown, when user signs Up with google method
 */
export const GoogleDecision = () => {
    const dispatch = useDispatch()
    const user = useSelector((state) => state.user)
    const [loader, setLoader] = useState(false)

    //After user makes his decision we signing him up into DB, showing him result for three results, and then redirect for the second auth
    const submitHandler = (e, type) => {
        e.preventDefault(e)
        dispatch(registerUser({ ...user, type: type }))
        setLoader(true)
        setTimeout(() => {
            dispatch(googleAuth())
        }, 3000)
    }

    const signOutHandler = (e) => {
        e.preventDefault()
        dispatch(signOutUser())
    }

    return (
        <>
            {loader ? (
                <>
                    <p>
                        We need to re-login you in order of security and bla bla
                        bla
                    </p>
                </>
            ) : (
                <>
                    <p>Now please decide which pill to take?</p>
                    <LargeButton
                        name={'We are Company!'}
                        onClick={(e) => submitHandler(e, 'employer')}
                    />
                    <LargeButton
                        name={'I am seeking new job!'}
                        onClick={(e) => submitHandler(e, 'employee')}
                    />
                    <p>Can't decide?</p>
                    <InputButton
                        name={'Sign Out'}
                        onClick={(e) => signOutHandler(e)}
                    />
                </>
            )}
        </>
    )
}
