import { MDBCol, MDBCollapse, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import InputButton from '../../../../assets/Buttons/InputButton'
import { ReactComponent as ComputerLight } from '../../../../assets/images/ComputerLight.svg'
import { googleAuth } from '../../../../services/userDispatcher'
import SignInForm from './SignInForm'
import styled from 'styled-components'

const SignInContainer = styled.div`
    display: flex;
    width: auto;
    max-width: 300px;
    flex-direction: column;
    background: ${({ theme }) => theme.color.bg};
    padding: 2rem;
    margin: 0 auto;
`

export default function SignIn() {
    const dispatch = useDispatch()
    // Show or hide email sign in option:
    const [collapse, setCollapse] = useState()
    const toggleCollapse = () => setCollapse((prevState) => !prevState)

    const googleHandler = (e) => {
        e.preventDefault()
        dispatch(googleAuth())
    }

    return (
        <MDBContainer fluid className="d-flex justify-content-center py-5">
            <MDBRow>
                <MDBCol className="d-flex-column">
                    <SignInContainer>
                        <h3 className="text-center">Sign in Silta with</h3>
                        <InputButton name="Email" onClick={toggleCollapse} />
                        <MDBCollapse show={collapse}>
                            <SignInForm className="mb-5" />
                        </MDBCollapse>
                        <InputButton
                            onClick={(e) => {
                                googleHandler(e)
                            }}
                            name="Google"
                        />
                        <InputButton name="LinkedIn" />
                    </SignInContainer>
                </MDBCol>
                <MDBCol sm="12" lg="6">
                    <ComputerLight className="w-100 p-5 d-none d-sm-block align-self-center computerLight" />
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    )
}
