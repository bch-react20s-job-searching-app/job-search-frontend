import { MDBContainer } from 'mdb-react-ui-kit'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router'
import InputButton from '../../../../assets/Buttons/InputButton'
import InputField from '../../../../assets/InputFields/InputField'
import signInErrors from '../../../../constants/signInErrors'
import signInInputs from '../../../../constants/signInInputs'
import { signInUser } from '../../../../services/userDispatcher'
import { signIn } from '../../../../services/validator'

export default function SignInForm() {
    const dispatch = useDispatch()
    const history = useHistory()

    const [inputs] = useState(signInInputs)
    const [errorsMsg] = useState(signInErrors)

    //Initizalizing form state object
    const [form, setForm] = useState({})

    //Initializing error state array
    const [err, setErr] = useState([])

    const [msg, setMsg] = useState('')

    const handleSubmit = (e) => {
        e.preventDefault()
        setErr([])
        const validationResult = signIn(form)
        if (validationResult.length > 0) {
            setErr(validationResult)
            return
        }
        dispatch(signInUser(form)).then((r) => {
            r !== 'authenticated' ? setMsg(r) : history.push('/')
        })
    }

    const handleChange = (e) => {
        setForm({ ...form, [e.target.name]: e.target.value })
    }

    return (
        <MDBContainer className="d-flex-column">
            <form>
                {inputs.map((input) => (
                    <InputField
                        key={input.id}
                        name={input.name}
                        id={input.id}
                        type={input.type}
                        onChange={(e) => {
                            handleChange(e)
                        }}
                        err={err.includes(input.id)}
                    />
                ))}
                <p
                    style={{
                        color: 'red',
                        padding: '0',
                        fontSize: '12px',
                        marginBottom: '0.1rem',
                    }}
                    className="d-flex flex-row justify-content-center"
                >
                    {err.length > 0 &&
                        errorsMsg.filter((e) => e.type === err[0])[0].message}
                    {msg}
                </p>

                <InputButton name={'Sign in'} onClick={handleSubmit} />
            </form>
        </MDBContainer>
    )
}
