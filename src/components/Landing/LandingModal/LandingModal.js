import React, { useState } from 'react';
import { MDBCol, MDBBtn, MDBModal, MDBModalDialog, MDBModalContent, MDBModalHeader } from 'mdb-react-ui-kit';
import LargeButton from '../../../assets/Buttons/LargeButton'
import ModalContent from './ModalContent/ModalContent'
import styled from 'styled-components'

const StyledDiv = styled.div `
    color: ${({theme}) => theme.color.accent2.main};
    font-size: ${({theme}) => theme.font.size.inlineLink};
`
const StyledModalCont = styled.div `
    background: ${({theme}) => theme.color.darkShadow};
`
const StyledTitle = styled.h2 `
    color: ${({theme}) => theme.color.accent2.main};
`

export default function LandingModal() {
  
  const [modal1, setModal1] = useState(false);
  const toggleShow = () => setModal1(!modal1);

  return (
    <>
      {/* Show only inline link to modal on small screens */}
      <MDBCol className='d-md-none'>
          <StyledDiv onClick={toggleShow}>How it works?</StyledDiv>
      </MDBCol>
      <MDBCol className='d-none d-md-block'>
          <LargeButton onClick={toggleShow} name="How it works"/>
      </MDBCol>
      <StyledModalCont>     
      <MDBModal className='modalContainer' tabIndex='-1' show={modal1} getOpenState={(e) => setModal1(e)}>
        <MDBModalDialog centered className='w-75' role='dialog' style={{margin: '0 auto'}}>
          <MDBModalContent className='p-4'>
            <MDBModalHeader>
              <StyledTitle className='col-md-11' tag='h2'>How it works?</StyledTitle>              
              <MDBBtn className='col-md-1 btn-close' color='none' onClick={toggleShow}></MDBBtn>
            </MDBModalHeader>
            <ModalContent/>
          </MDBModalContent>
        </MDBModalDialog>
      </MDBModal>
      </StyledModalCont> 
    </>
  );
}
