import React, { useState } from 'react'
import {
    modalHeading1,
    modalHeading2,
    modalHeading3,
    modalText1,
    modalText2,
    modalText3,
} from '../../../../constants/landing.json'
import styled from 'styled-components'

const ArrowButton = styled.button `
    border-radius: 50%;
    width: 3rem;
    height: 3rem;
    border: none;
    padding: .5rem;
    background: ${({theme}) => theme.color.accent1.main};

    i   {
        color: ${({theme}) => theme.color.lightShadow};
        margin: auto 0;
    }

    &:disabled {
    background-color: ${({theme}) => theme.color.darkShadow};
    }
`

export default function ModalContent() {
    const modalContent = [
        { id: 1, heading: modalHeading1, text: modalText1 },
        { id: 2, heading: modalHeading2, text: modalText2 },
        { id: 3, heading: modalHeading3, text: modalText3 },
    ]

    const [currentContent, setCurrentContent] = useState(0)

    const prevContent = () => {
        setCurrentContent(currentContent - 1)
    }

    const nextContent = () => {
        setCurrentContent(currentContent + 1)
    }

    const activeContent = modalContent.slice(currentContent, currentContent + 1)

    return (
        <div>
            {activeContent.map((modalContent) => (
                <div key={modalContent.id}>
                    <h3 className="my-3">{modalContent.heading}</h3>
                    <p className="mb-4">{modalContent.text}</p>
                </div>
            ))}
            <ArrowButton
                className="m-4"
                onClick={prevContent}
                disabled={currentContent < 1}
            >
                <i className="fas fa-arrow-left"></i>
            </ArrowButton>
            <ArrowButton
                className="m-4"
                onClick={nextContent}
                disabled={currentContent > 1}
            >
                <i className="fas fa-arrow-right"></i>
            </ArrowButton>
        </div>
    )
}
