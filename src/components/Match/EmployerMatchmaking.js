import {
    relevantEmployees,
    retrieveAllEmployees,
} from '../../services/match.service'
import React, { useState, useEffect } from 'react'
import  { Link } from 'react-router-dom'
import EmployeeCard from './Jobs/ShowEmployees'

import styled from 'styled-components'

const Styledp = styled.p`
    padding: 0;
    margin-bottom: 0.5rem;
`
const StyledSpan = styled.span`
    color: ${({ theme }) => theme.color.accent1.main};
    padding: 0;
    font-size: 0.9rem;
`

export default function EmployerMatchmaking(props) {

    const [employees, setEmployees] = useState([])

    const [allemployees, setAllEmployees] = useState([])

    const [loading, setLoading] = useState(true) //UPDATE

    const selectedJob = props.selectedJob

    const [currentEmployee, setCurrentEmployee] = useState(0)

    // TODO: remove error 'React Hook useEffect has a missing dependency: 'userId'. Either include it or remove the dependency array  react-hooks/exhaustive-deps'

    const incrementEmployee = () => {
        setCurrentEmployee(currentEmployee + 1)
    }

    useEffect(() => {
        relevantEmployees(setEmployees, setLoading, selectedJob)
        retrieveAllEmployees(setAllEmployees)
    }, [setEmployees, setLoading])

    return (
        <div>
            {(function () {
                if (loading) {
                    return <p>Loading..</p>
                } else if (employees.length === 0 || currentEmployee === employees.length) {
                    return (
                        <div className="text-center">                            
                            <h2>Thats all for now...</h2>
                            <Link to='/'>
                                <span>Come back in a while and check out newest Users</span>
                            </Link>
                        </div>
                    )
                } else if (!loading && employees.length !== 0 && currentEmployee < employees.length) {
                    return (
                        <>
                            <div className="text-center">
                                <Styledp>
                                    Your search returned{' '}
                                    <StyledSpan>
                                        {allemployees.length}
                                    </StyledSpan>{' '}
                                    users.{' '}
                                </Styledp>
                                <Styledp>
                                    <StyledSpan>Like </StyledSpan>to add to your
                                    short list and show interest towards the
                                    user.{' '}
                                </Styledp>
                                <Styledp>
                                    <StyledSpan>GigaLike </StyledSpan>to show
                                    that your are giga interested.
                                </Styledp>
                                <Styledp>
                                    <StyledSpan>Dislike </StyledSpan>if you don't wan't to see the user in your short list.
                                </Styledp>
                            </div>
                            <EmployeeCard
                                employee={employees[currentEmployee]}
                                incrementEmployee={incrementEmployee}
                                jobId={selectedJob}
                            />
                        </>
                    )
                }
            })()}
        </div>
    )
}
