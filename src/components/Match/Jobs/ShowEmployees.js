import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit'
import React from 'react'
import { useSelector } from 'react-redux'
import { likeEmployee } from '../../../services/match.service'
import styled from 'styled-components'

const StyledCard = styled.div`
    background-color: ${({ theme }) => theme.color.bg};
    width: 310px;
    height: auto;
    border-radius: 20px;
    box-shadow: inset -6px -6px 6px
            ${({ theme }) => theme.color.lightShadow},
        inset 4px 4px 4px ${({ theme }) => theme.color.darkShadow};
    border: solid 0.5px ${({ theme }) => theme.color.darkShadow};
    padding: 2rem;
    margin: 3rem auto;
`

export default function EmployeeCard(props) {
   
    const user = useSelector((state) => state.user)

    return (
        <MDBContainer>
            <StyledCard>
                <MDBRow className="mb-3">
                    <MDBCol className="d-flex-column justify-content-between">
                        <h2>{props.employee.name}</h2>
                    </MDBCol>
                    <MDBCol className="align-self-center">
                        <img
                            src="https://source.unsplash.com/FyD3OWBuXnY"
                            className="testPicture"
                            alt="testPicture"
                            style={{
                                width: '70px',
                                height: '70px',
                                borderRadius: '50%',
                            }}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="mb-3">
                    <MDBCol className="d-flex">
                        <label>Name</label>
                        <p>{props.employee.firstname}</p>
                    </MDBCol>
                </MDBRow>
                <MDBRow className="mb-3">
                    <label>Skills</label>
                    <p>{props.employee.skills}</p>
                </MDBRow>
                <MDBRow className="mb-3">
                    <label>In addition, I have interest in</label>
                    <p>{props.employee.interests}</p>
                </MDBRow>
                <MDBRow className="mb-3">
                    <MDBCol className="d-flex">
                        <label>Years of experience</label>
                        <p>{props.employee.yearsOfExperience}</p>
                    </MDBCol>
                </MDBRow>
                <MDBRow className="mb-3">
                    <label>More about me</label>
                    <p>{props.employee.description}</p>
                </MDBRow>
                <MDBRow className="d-flex">
                    <MDBCol>
                        <button
                            onClick={() => {
                                props.incrementEmployee()
                                likeEmployee(
                                    props.employee._id.toString(),
                                    user._id.toString()
                                )
                            }}
                        >
                            <i className="far fa-thumbs-up"></i>
                        </button>
                    </MDBCol>
                    <MDBCol>
                        <i className="far fa-grin-hearts"></i>
                    </MDBCol>
                </MDBRow>
            </StyledCard>
        </MDBContainer>
    )
}
