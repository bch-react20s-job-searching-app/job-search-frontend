import React, { useState } from 'react'
import EmployerMatchmaking from '../EmployerMatchmaking'
import { useSelector } from 'react-redux'

const CurrentJob = () => {
    const [selectedJob, setSelectedJob] = useState('')

    const user = useSelector((state) => state.user)

    return (
        <div>
            {(function () {
                if (selectedJob === '') {
                    return (
                        <div className="users">
                            <h1>Please select which Job to search for..</h1>
                            {user.jobs.map((job) => (
                                <div className="user">
                                    {job}
                                    <button
                                        onClick={() => {
                                            setSelectedJob(job)
                                        }}
                                    >
                                        hello
                                    </button>
                                </div>
                            ))}
                        </div>
                    )
                } else {
                    return <EmployerMatchmaking selectedJob={selectedJob} />
                }
            })()}
        </div>
    )
}

export default CurrentJob
