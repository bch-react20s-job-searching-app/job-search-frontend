import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import { likeJob } from '../../../services/match.service'

const StyledCard = styled.div`
    background-color: ${({ theme }) => theme.color.bg};
    width: 310px;
    height: auto;
    border-radius: 20px;
    box-shadow: inset -6px -6px 6px ${({ theme }) => theme.color.lightShadow},
        inset 4px 4px 4px ${({ theme }) => theme.color.darkShadow};
    border: solid 0.5px ${({ theme }) => theme.color.darkShadow};
    padding: 2rem;
    margin: 3rem auto;
`

export default function JobCard(props) {
    const user = useSelector((state) => state.user)
    return (
        <MDBContainer>
            <StyledCard>
                <MDBRow className="mb-3">
                    <MDBCol className="d-flex-column justify-content-between">
                        <h2>{props.job.title}</h2>
                        <h3>TestCompany</h3>
                    </MDBCol>
                    <MDBCol className="align-self-center">
                        <img
                            src="https://source.unsplash.com/FyD3OWBuXnY"
                            className="testPicture"
                            alt="testPicture"
                            style={{
                                width: '70px',
                                height: '70px',
                                borderRadius: '50%',
                            }}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="mb-3">
                    <MDBCol className="d-flex">
                        <label>Seniority</label>
                        <p>{props.job.seniority}</p>
                    </MDBCol>
                </MDBRow>
                <MDBRow className="mb-3">
                    <label>Skills you should possess</label>
                    <p>{props.job.requiredSkills}</p>
                </MDBRow>
                <MDBRow className="mb-3">
                    <label>In addition, we value interest in</label>
                    <p>{props.job.desirableSkills}</p>
                </MDBRow>
                <MDBRow className="mb-3">
                    <MDBCol className="d-flex">
                        <label>Contract type</label>
                        <p>{props.job.contractType}</p>
                    </MDBCol>
                </MDBRow>
                <MDBRow className="mb-3">
                    <label>More about the job</label>
                    <p>{props.job.description}</p>
                </MDBRow>
                <MDBRow className="d-flex">
                    <MDBCol>
                        <button
                            style={{ border: 'none', background: 'none' }}
                            onClick={() => {
                                props.incrementJob()
                                likeJob(
                                    props.job._id.toString(),
                                    user._id.toString()
                                )
                            }}
                        >
                            <i className="far fa-thumbs-up"></i>
                        </button>
                    </MDBCol>
                    <MDBCol>
                        <i className="far fa-grin-hearts"></i>
                    </MDBCol>
                    <MDBCol>
                        <i className="far fa-thumbs-down"></i>
                    </MDBCol>                    
                </MDBRow>
            </StyledCard>
        </MDBContainer>
    )
}
