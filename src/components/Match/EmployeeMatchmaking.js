import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import  { Link } from 'react-router-dom'
import JobCard from './Jobs/ShowJob'
import { relevantJobs, retrieveAllJobs } from '../../services/match.service'
import styled from 'styled-components'

const Styledp = styled.p`
    padding: 0;
    margin-bottom: 0.5rem;
`
const StyledSpan = styled.span`
    color: ${({ theme }) => theme.color.accent1.main};
    padding: 0;
    font-size: 0.9rem;
`

export default function EmployeeMatchmaking() {
    const user = useSelector((state) => state.user)

    const [jobs, setJobs] = useState([])

    const [allJobs, setAllJobs] = useState([])

    const [loading, setLoading] = useState(true)

    const [currentJob, setCurrentJob] = useState(0)

    // TODO: remove error 'React Hook useEffect has a missing dependency: 'userId'. Either include it or remove the dependency array  react-hooks/exhaustive-deps'
    const userId = user._id.toString()

    const incrementJob = () => {
        setCurrentJob(currentJob + 1)
    }

    useEffect(() => {
        relevantJobs(setJobs, setLoading, userId)
        retrieveAllJobs(setAllJobs)
    }, [setJobs, setLoading]) // only run when setJobs value is changed

    console.log(user._id)
    console.log(allJobs)

    return (
        <div>
            {(function () {
                if (loading) {
                    return <p>Loading..</p>
                } else if (jobs.length === 0 || currentJob === jobs.length) {
                    return (
                        <div className="text-center">
                            <h2>Thats all for now...</h2>
                            <Link to='/'>
                                <span>Come back in a while and check out newest job openings</span>
                            </Link>                            
                        </div>
                    )
                } else if (!loading && jobs.length !== 0 && currentJob < jobs.length) {
                    return (
                        <>
                            <div className="text-center">
                                <Styledp>
                                    Your search returned{' '}
                                    <StyledSpan>{allJobs.length}</StyledSpan>{' '}
                                    open jobs.{' '}
                                </Styledp>
                                <Styledp>
                                    <StyledSpan>Like </StyledSpan>to add to your
                                    short list and show interest towards the
                                    company.{' '}
                                </Styledp>
                                <Styledp>
                                    <StyledSpan>GigaLike </StyledSpan>to show
                                    that your are giga interested.
                                </Styledp>
                                <Styledp>
                                    <StyledSpan>Dislike </StyledSpan>if you don't wan't to see the job in your short list.
                                </Styledp>
                            </div>
                            <JobCard
                                job={jobs[currentJob]}
                                incrementJob={incrementJob}
                                userId={userId}
                            />
                        </>
                    )
                }
            })()}
        </div>
        /* 
        <div>
            if (loading) {<p>Loading...</p>}
            else if (!loading)
            {
                <JobCard
                    job={jobs[currentJob]}
                    incrementJob={incrementJob}
                    userId={userId}
                />
            }
            else
        </div>
        */
    )
}
