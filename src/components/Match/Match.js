import React from 'react'
import EmployeeMatchmaking from './EmployeeMatchmaking'
import CurrentJob from './Jobs/CurrentJob'
import { useSelector } from 'react-redux'

const Match = () => {
    const user = useSelector((state) => state.user)
    return <div>{user.employer ? <CurrentJob /> : <EmployeeMatchmaking />}</div>
}

export default Match
