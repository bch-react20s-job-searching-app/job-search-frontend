import React from 'react'
import { Link } from 'react-router-dom'
import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit'
import { ReactComponent as WomanOnCompu } from '../../assets/images/WomanOnCompu.svg'

export default function JobseekerMain() {
    return (
        <>
            <MDBContainer className="p-1 text-center">
                <MDBRow>
                    <h1>Welcome to Silta!</h1>
                </MDBRow>
                <MDBRow className="align-items-center">
                    <MDBCol lg="6" md="12" className="pb-5">
                        <div className="m-5">
                            <h2>What would you like to do?</h2>
                        </div>
                        <div className="p-4">
                            <Link to="/profile">
                                <span>View and edit your profile?</span>
                            </Link>
                        </div>
                        <div>
                            <Link to="/matchmaking" className="p-4">
                                <span>Checkout interesting job offers?</span>
                            </Link>
                        </div>
                        <div className="p-4">
                            <Link to="/messages">
                                <span>
                                    View your matches and start a chat with
                                    them?
                                </span>
                            </Link>
                        </div>
                    </MDBCol>
                    <MDBCol
                        lg="6"
                        md="12"
                        className="p-3 mb-5 d-none d-md-block"
                    >
                        <WomanOnCompu
                            className="mb-3"
                            style={{ width: '100%' }}
                        />
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </>
    )
}
