import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import InputButton from '../../../../../assets/Buttons/InputButton'
import InputField from '../../../../../assets/InputFields/InputField'
import EmployeeCountSelect from '../../../../../assets/InputFields/Selects/EmployeeCountSelect'
import { employeeCountOptions } from '../../../../../constants/selectOptions'
import { updateUser } from '../../../../../services/userDispatcher'

export default function FormCompanyProfile() {
    //Fetching user from global state
    const user = useSelector((state) => state.user)

    //Initializing form state
    const [formData, setFormData] = useState(user)

    const dispatch = useDispatch()

    useEffect(() => {
        setFormData(user)
    }, [user])

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(updateUser(formData))
    }

    return (
        <form onSubmit={handleSubmit}>
            <MDBContainer>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="Company ID (VAT no.)"
                            id="yTunnus"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Company website"
                            id="website"
                            type="url"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Company phone number"
                            id="companyPhoneNumber"
                            type="tel"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <EmployeeCountSelect
                            id="employeesCount"
                            name="employeesCount"
                            options={employeeCountOptions}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Field of business"
                            id="fieldOfWork"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Company primary language(s)"
                            id="primaryLanguage"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="Headquarters street address"
                            id="headQuartersStreet"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Postal code"
                            id="postalCode"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="City"
                            id="headQuartersCity"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Country"
                            id="country"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="Your first name"
                            id="hrFirstName"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Your last name"
                            id="hrLastName"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Job title"
                            id="hrTitle"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Your username"
                            id="username"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Phone number"
                            id="hrPhoneNumber"
                            type="tel"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="Email"
                            id="email"
                            type="email"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="Open jobs your company has listed in Silta"
                            id="openJobs"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="Password"
                            id="password"
                            type="password"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
            <MDBContainer className="d-flex justify-content-center mb-5">
                <InputButton type="submit" name="Submit" />
            </MDBContainer>
        </form>
    )
}
