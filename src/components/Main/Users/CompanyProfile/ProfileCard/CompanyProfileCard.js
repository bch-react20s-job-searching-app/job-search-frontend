import React from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit';
import styled from 'styled-components'

const StyledCard = styled.div`
  background-color: ${({ theme }) => theme.color.bg};
  width: 300px;
  height: auto;
  border-radius: 20px;
  box-shadow: inset -6px -6px 6px ${({ theme }) => theme.color.lightShadow}, inset 4px 4px 4px ${({ theme }) => theme.color.darkShadow};
  border: solid .5px ${({ theme }) => theme.color.darkShadow};
  padding: 2rem;
`

export default function CompanyProfileCard({user}) {
  return (  
      <MDBContainer className='d-flex align-items-center'>    
      <StyledCard>
          <MDBRow className='d-grid gap-2 mb-2'>
              <MDBCol className='d-flex justify-content-between'>           
                <h3 className='companyName'>Company name</h3> 
                <img
                  src='https://source.unsplash.com/FyD3OWBuXnY'
                  className='testPicture'
                  alt='testPicture'
                  style={{ width: '70px', height: '70px',  borderRadius: '50%' }}
                />                                    
            </MDBCol>  
            <MDBCol>
                <h4 className='country'>Country</h4>  
            </MDBCol>               
        </MDBRow>        
        <MDBRow  className='d-grid gap-2 mb-5'>
          <MDBCol>
              <label className='cardLabel'>We are currently looking for...</label>
              <p className='cardText'>List of first 3 jobs here... 1</p>
              <p className='cardText'>List of first 3 jobs here... 2</p>
              <p className='cardText'>List of first 3 jobs here... 3</p>
            </MDBCol>
          <MDBCol>
            <label className='cardLabel'>You can find us in</label>
            <p className='cardText'>url: company website, LinkedIn page...?</p>
          </MDBCol>          
        </MDBRow>
        <MDBRow  className='d-flex'>
          <MDBCol>
          <i className="far fa-thumbs-up"></i>
          </MDBCol>
          <MDBCol>
          <i className="far fa-grin-hearts"></i>
          </MDBCol>
          <MDBCol>
            <i className="far fa-thumbs-down"></i>
          </MDBCol>   
        </MDBRow>
        </StyledCard> 
      </MDBContainer>     
  );
}
