import React, { useState } from 'react'
import { MDBContainer, MDBRow, MDBCol, MDBModal, MDBModalDialog } from 'mdb-react-ui-kit';
import FormCompanyProfile from './Form/FormCompanyProfile'
import CompanyProfileCard from './ProfileCard/CompanyProfileCard'
import { useViewportWidth } from '../../../../hooks'

export default function CompanyProfile() {

    const { windowWidth } = useViewportWidth()
    const breakpoint = 768
    const [modal, setModal] = useState(false);
    const toggleShow = () => setModal(!modal);
 
	return (
        <MDBContainer>
            <MDBRow>
            { windowWidth < breakpoint ? (   
                   <>       
                    <MDBCol className='d-grid'>
                        <span className='align-self-end text-center' onClick={toggleShow}>Preview your company's  public profile</span>                   
                    </MDBCol> 
                    <MDBContainer>
                        <MDBModal tabIndex='-1' show={modal} getOpenState={(e) => setModal(e)}>
                            <MDBModalDialog centered className='d-flex align-items-start' role='dialog'> 
                            <CompanyProfileCard/>               
                            </MDBModalDialog>
                        </MDBModal>
                    </MDBContainer>  
                </> 
            )  : 
                <MDBCol className='d-grid' md='12' lg='4'>
                    <h3 className='align-self-end text-center'>Preview of your company's public profile</h3>
                    <div className='mx-auto align-self-start'>
                        <CompanyProfileCard/> 
                    </div>
                </MDBCol>        
            } 
                <MDBCol className='d-flex justify-content-center align-items-center' lg='8'>
                   <FormCompanyProfile/>                 
                </MDBCol>         
            </MDBRow>
        </MDBContainer>
    )
}

