import React, { useState } from 'react'
import  { Link } from 'react-router-dom'
import { MDBContainer, MDBRow, MDBCol, MDBModal, MDBModalDialog } from 'mdb-react-ui-kit';
import FormJobseekerProfile from './Form/FormJobseekerProfile'
import JobseekerProfileCard from './ProfileCard/JobseekerProfileCard'
import { useViewportWidth } from '../../../../hooks'

export default function JobseekerProfile() {
    
    const { windowWidth } = useViewportWidth()
    const breakpoint = 768
    const [modal, setModal] = useState(false);
    const toggleShow = () => setModal(!modal);

	return (
        <MDBContainer>
            <MDBRow> 
            { windowWidth < breakpoint ? (   
                   <>       
                    <MDBCol className='d-grid'>
                        <span className='align-self-end text-center' onClick={toggleShow}>Preview your public profile</span>                   
                    </MDBCol> 
                    <MDBContainer>
                        <MDBModal tabIndex='-1' show={modal} getOpenState={(e) => setModal(e)}>
                            <MDBModalDialog centered className='d-flex align-items-start' role='dialog'> 
                                <JobseekerProfileCard/>                
                            </MDBModalDialog>
                        </MDBModal>
                    </MDBContainer>  
                </> 
            )  : 
                <MDBCol className='d-grid' md='12' lg='4'>
                    <h3 className='align-self-end text-center'>Preview of your public profile</h3>
                    <div className='mx-auto align-self-start'>
                        <JobseekerProfileCard/>
                        <Link to='/profile' className='p-4'><span>Back to profile page</span></Link>
                    </div>
                </MDBCol>        
            }                              
                <MDBCol className='d-flex justify-content-center' md='12' lg='8'>
                   <FormJobseekerProfile/>               
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    )
}

