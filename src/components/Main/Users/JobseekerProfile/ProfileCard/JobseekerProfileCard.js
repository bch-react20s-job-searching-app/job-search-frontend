import React from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit';
import { useSelector } from 'react-redux';
import styled from 'styled-components'

const StyledCard = styled.div`
  background-color: ${({ theme }) => theme.color.bg};
  width: 300px;
  height: auto;
  border-radius: 20px;
  box-shadow: inset -6px -6px 6px ${({ theme }) => theme.color.lightShadow}, inset 4px 4px 4px ${({ theme }) => theme.color.darkShadow};
  border: solid .5px ${({ theme }) => theme.color.darkShadow};
  padding: 2rem;
  margin-bottom: 2rem;
`
const StyledP = styled.p`
   padding-left: 0;
   font-weight: bold;
   margin-bottom: 1rem;
`

export default function JobseekerProfileCard() {
  const user = useSelector((state) => state.user)

  return (      
        <MDBContainer className='d-flex align-items-center'> 
          <StyledCard>
          <MDBRow  className='d-grid gap-2 mb-5'>
            <MDBCol>
              <label className='cardLabel'>I'm good at</label>
              <StyledP className='cardText'>{user.skills}</StyledP>
            </MDBCol>
            <MDBCol>
              <label className='cardLabel'>I'm interested in</label>
              <StyledP className='cardText'>{user.interests}</StyledP>
            </MDBCol>          
            <MDBCol>
              <label className='cardLabel'>Who I am</label>
              <StyledP className='cardText'>{user.description}</StyledP>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol className='d-flex justify-content-between mb-3'>           
                <h3 className='username'>{user.username}</h3> 
                <h4 className='country'>{user.country}</h4>                       
            </MDBCol>      
          </MDBRow>
          <MDBRow  className='d-flex'>
            <MDBCol>
              <i className="far fa-thumbs-up"></i>
            </MDBCol>
            <MDBCol>
              <i className="far fa-grin-hearts"></i>
            </MDBCol>
            <MDBCol>
              <i className="far fa-thumbs-down"></i>
            </MDBCol>   
          </MDBRow> 
          </StyledCard> 
        </MDBContainer>     
       
  );
}
