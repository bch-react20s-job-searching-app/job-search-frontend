import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const StyledP = styled.p`
   padding-left: 0;
   font-weight: bold;
`

export default function MainJobseekerProfile() {
    const user = useSelector((state) => state.user)

    return (
        <div>
            <MDBContainer className='mx-5'>
                <div className='d-flex flex-column mb-5 w-300'>                         
                <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol>  
                            <Link to='/profile/edit'>
                                <span style={{paddingLeft: 0}}>Edit profile</span>
                            </Link>
                        </MDBCol>
                    </MDBRow> 
                    <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol>
                            <img src={user.image} alt="User avatar/pic"></img>
                        </MDBCol>
                        <MDBCol>
                            <StyledP>{user.DOB.slice(0,10)}</StyledP>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol>
                            <label>First Name</label>
                            <StyledP>{user.firstname}</StyledP>
                        </MDBCol>
                        <MDBCol>
                            <label>Last Name</label>
                            <StyledP>{user.lastname}</StyledP>
                        </MDBCol>             
                    </MDBRow>
                    <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol>
                            <label>Email</label>
                            <StyledP>{user.email}</StyledP>
                        </MDBCol>
                        <MDBCol>
                            <label>Telephone number</label>
                            <StyledP>{user.phoneNumber}</StyledP>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol>
                            <label>City</label>
                            <StyledP>{user.city}</StyledP>
                        </MDBCol>
                        <MDBCol>
                            <label>Country</label>
                            <StyledP>{user.country}</StyledP>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol>
                            <label>Level</label>
                            <StyledP>{user.seniority}</StyledP>
                        </MDBCol>
                        <MDBCol>
                            <label>Experience</label>
                            <StyledP>{user.yearsOfExperience}</StyledP>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol>
                            <label>Skills</label>
                            <StyledP>{user.skills}</StyledP>
                        </MDBCol>
                        <MDBCol>  
                            <label>Interests</label>
                            <StyledP>{user.interests}</StyledP>
                        </MDBCol>
                    </MDBRow> 
                    <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol> 
                            <label>Languages</label> 
                            <StyledP>{user.languages}</StyledP>
                        </MDBCol>
                    </MDBRow> 
                    <MDBRow className='col-sm-6 mb-4 w-100'>
                        <MDBCol>  
                            <label>A little about me</label>
                            <StyledP>{user.description}</StyledP>
                        </MDBCol>
                    </MDBRow>            
                </div>
            </MDBContainer>
        </div>
    )
}
