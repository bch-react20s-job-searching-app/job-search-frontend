import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import InputButton from '../../../../../assets/Buttons/InputButton'
import InputField from '../../../../../assets/InputFields/InputField'
import TextArea from '../../../../../assets/InputFields/TextArea'
import { updateUser } from '../../../../../services/userDispatcher'
// import SkillsSelect from '../../../../../assets/InputFields/Selects/SkillsSelect'
// import SenioritySelect from '../../../../../assets/InputFields/Selects/SenioritySelect'
// import ExperienceSelect from '../../../../../assets/InputFields/Selects/SenioritySelect'
// import { seniorityOptions, experienceOptions, skillsOptions } from '../../../../../constants/selectOptions';

export default function FormJobseekerProfile() {
    const dispatch = useDispatch()

    //Initializing local state for the form
    const [formData, setFormData] = useState({})

    const handleChange = (e) => {
        //If this input has been attached with file we returning setFormData, but with fixed key of the value, and different e.target value
        if (e.target.files)
            return setFormData({ ...formData, photo: e.target.files[0] })

        //By default we just update the formData state
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        //Those scripts are run, when formData has one of those fields
        //It splits the string we get from input into array of strings
        //The define symbol is comma
        formData?.skills &&
            (formData.skills = [...formData.skills.toString().split(',')])

        formData?.languages &&
            (formData.languages = [...formData.languages.toString().split(',')])

        formData?.interests &&
            (formData.interests = [...formData.interests.toString().split(',')])

        dispatch(updateUser(formData))
    }

    const user = useSelector((state) => state.user)

    return (
        <form onSubmit={handleSubmit}>
            <MDBContainer>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="firstname"
                            labelMsg="First Name"
                            id="firstname"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="lastname"
                            labelMsg="Last name"
                            id="lastname"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="username"
                            labelMsg="Username"
                            id="username"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="DOB"
                            labelMsg="Date of birth"
                            id="DOB"
                            type="date"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="phonenumber"
                            labelMsg="Phone number (please include country code)"
                            id="phoneNumber"
                            type="tel"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    {/* <MDBCol md="6">
                        <InputField
                            name="email"
                            labelMsg="Email"
                            id="email"
                            type="email"
                            onChange={handleChange}
                        />
                    </MDBCol> */}
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="city"
                            labelMsg="City"
                            id="city"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="country"
                            labelMsg="Country"
                            defaultValue={user.country}
                            id="country"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        {/* <SenioritySelect id="seniority" labelMsg="Seniority" name="seniority" options={seniorityOptions} onChange={handleChange}></SenioritySelect> */}
                        <InputField
                            name="seniority"
                            labelMsg="Seniority"
                            id="seniority"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        {/* <SkillsSelect id='skills' labelMsg="Skills" name='skills' options={skillsOptions} onChange={handleChange}/> */}
                        <InputField
                            name="skills"
                            labelMsg="Skills, please separate with a comma"
                            id="skills"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        {/* <ExperienceSelect id='yearsOfExperience' name='yearsOfExperience' options={experienceOptions} onChange={handleChange}/> */}
                        <InputField
                            name="experience"
                            labelMsg="Experience (in years)"
                            id="experience"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="links"
                            labelMsg="Links"
                            id="links"
                            type="url"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            labelMsg="Interests, please separate with a comma"
                            name="interests"
                            id="interests"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="languages"
                            labelMsg="Languages, please separate with a comma"
                            id="languages"
                            type="text"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <MDBCol md="6">
                        <InputField
                            name="password"
                            labelMsg="Password"
                            id="password"
                            type="password"
                            onChange={handleChange}
                        />
                    </MDBCol>
                    <MDBCol md="6">
                        <InputField
                            name="photo"
                            labelMsg="Image"
                            id="photo"
                            type="file"
                            onChange={handleChange}
                        />
                    </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex col-sm-6 mb-1 w-100">
                    <TextArea
                        labelMsg="Describe yourself"
                        defaultValue="{user.description}"
                        name="description"
                        id="description"
                        type="text"
                        onChange={handleChange}
                    />
                </MDBRow>
            </MDBContainer>
            <MDBContainer className="d-flex justify-content-center mb-5">
                <InputButton type="submit" name="Submit" />
            </MDBContainer>
        </form>
    )
}
