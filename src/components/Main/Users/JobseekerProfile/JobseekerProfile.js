import React, { useState } from 'react'
import {
    MDBContainer,
    MDBRow,
    MDBCol,
    MDBModal,
    MDBModalDialog,
} from 'mdb-react-ui-kit'
import JobseekerProfileCard from './ProfileCard/JobseekerProfileCard'
import MainJobseekerProfile from './MainJobseekerProfile/MainJobseekerProfile'
import { useViewportWidth } from '../../../../hooks'

export default function JobseekerProfile() {
    const { windowWidth } = useViewportWidth()
    const breakpoint = 768
    const [modal, setModal] = useState(false)
    const toggleShow = () => setModal(!modal)

    return (
        <MDBContainer>
            <MDBRow> 
            { windowWidth < breakpoint ? (   
                   <>       
                    <MDBCol className='d-grid'>
                        <span className='align-self-end text-center' onClick={toggleShow}>Preview your public profile</span>                   
                    </MDBCol> 
                    <MDBContainer>
                        <MDBModal tabIndex='-1' show={modal} getOpenState={(e) => setModal(e)}>
                            <MDBModalDialog centered className='mt-5 d-flex align-items-start' role='dialog'> 
                                <JobseekerProfileCard/>                
                            </MDBModalDialog>
                        </MDBModal>
                    </MDBContainer>  
                </> 
            )  : 
                <MDBCol className='d-grid' md='12' lg='4'>
                    <h3 className='align-self-end text-center'>Preview of your public profile</h3>
                    <div className='mx-auto align-self-start'>
                        <JobseekerProfileCard/>
                    </div>
                </MDBCol>        
            }                              
                <MDBCol className='align-self-end' md='12' lg='8'>
                   <MainJobseekerProfile/>               
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    )
}
