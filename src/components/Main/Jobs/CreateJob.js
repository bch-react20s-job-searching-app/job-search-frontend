import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router'
import InputButton from '../../../assets/Buttons/InputButton'
import InputField from '../../../assets/InputFields/InputField'
import createJobErrors from '../../../constants/createJobErrors'
import createJobInputs from '../../../constants/createJobInputs'
import { addJob } from '../../../services/userDispatcher'
import { createJob } from '../../../services/validator'

export const CreateJob = () => {
    const dispatch = useDispatch()
    const history = useHistory()

    //Initializing input state, as a default state -> createJobInputs from another file
    const [inputs] = useState(createJobInputs)

    //Initializing error messages state, as a default state -> createJobErrors from another file
    const [errorsMsg] = useState(createJobErrors)

    //Initizalizing form state object
    const [form, setForm] = useState({ possibilityRemote: false })

    //Initializing error state array
    const [err, setErr] = useState([])

    //Initializing info message state
    const [msg, setMsg] = useState('')

    const handleSubmit = (e) => {
        e.preventDefault()

        //refreshing error state
        setErr([])

        //Checking form again
        const validationResult = createJob(form)
        if (validationResult.length > 0) {
            setErr(validationResult)
            return
        }

        //If everything ok -> dispatching form to the dispatcher
        dispatch(addJob(form)).then((r) => {
            //After success job creation redirects you back
            r !== 'Job created!' ? setMsg(r) : history.goBack()
        })
    }

    const handleChange = (e) => {
        //If this input has been attached with file we returning setForm, but with fixed key of the value, and different e.target value
        if (e.target.files)
            return setForm({ ...form, photo: e.target.files[0] })

        //Handler for checkbox inputs, it toggles predefined values of those checkbox inputs in state
        if (e.target.type === 'checkbox')
            return setForm({
                ...form,
                [e.target.name]: !form[e.target.name],
            })
        //By default we just update the form state
        setForm({ ...form, [e.target.name]: e.target.value })
    }

    return (
        <MDBContainer>
            <MDBRow>
                <MDBCol md="8" lg="6" className="text-center">
                    <h3>Create a job add</h3>
                </MDBCol>
            </MDBRow>
            <MDBRow>
                <MDBCol md="8" lg="6" className="mb-1">
                    <form>
                        {inputs.map((input) => (
                            <InputField
                                key={input.id}
                                labelMsg={input.labelMsg}
                                name={input.name}
                                id={input.id}
                                type={input.type}
                                onChange={(e) => {
                                    handleChange(e)
                                }}
                                err={err.includes(input.id)}
                            />
                        ))}
                        <p
                            style={{
                                color: 'red',
                                padding: '0',
                                fontSize: '12px',
                                marginBottom: '0.1rem',
                            }}
                            className="d-flex flex-row justify-content-center"
                        >
                            {err.length > 0 &&
                                errorsMsg.filter((e) => e.type === err[0])[0]
                                    .message}
                            {msg}
                        </p>
                        <InputButton name={'Submit'} onClick={handleSubmit} />
                    </form>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    )
}
