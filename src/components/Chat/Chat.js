import {
    MDBCol,
    MDBContainer,
    MDBRow
} from 'mdb-react-ui-kit'
import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import  { Link } from 'react-router-dom'
import { useChat } from '../../services/socketio.service'
import TextArea from '../../assets/InputFields/TextArea'
import styled from 'styled-components'

const ChatHeader = styled.div `
    height: 100px;
    width: 100vw;
    position: fixed;
    top: 0;
    left: 0;
    background: ${({ theme }) => theme.color.bg};
    padding: 5rem 0;
`
const MessagesArea = styled.div `
    background-color: ${({ theme }) => theme.color.bg};
    border-bottom: 2px solid red;
    width: 380px;
    height: 60vh;
    position: fixed;
    top: 150px;
    padding: 1.5rem;
    margin: 0 auto;
    overflow: auto;
    @media (max-width: 400px) {
        width: 90vw;
        box-shadow: none;
        border: none;
        height: 80%;
        overflow: scroll;
    }
`
const Message = styled.div `
    border: 0.5px solid ${({ theme }) => theme.color.darkShadow};
    border-radius: 20px;
    padding: 10px 15px;
    margin: 0 20px;
    max-width: 250px;
    font-family: ${({ theme }) => theme.font.style.text};
    font-size: 0.85rem;
    font-weight: 600;
`
const WriteMessageArea = styled.div `
    height: 100px;
    width: 400px;
    position: fixed;
    bottom: 140px;
    margin: 2rem auto 1.5rem 3rem;
    background: ${({ theme }) => theme.color.bg};
    @media (max-width: 400px) {
        width: 90vw;
        bottom: 0;      
    }
`
//TODO: Ensure only newest 20 or so messages are shown, set anchor so page scrolls to bottom and new room for each match allow enter to send message

export default function Chat(props) {
    const { roomId } = 1 //TODO: set to props.match.params once matching works // Gets roomId from URL
    const { messages, sendMessage } = useChat(roomId) // Creates a websocket and manages messaging
    const [newMessage, setNewMessage] = useState('') // Message to be sent
    const user = useSelector((state) => state.user)

    // console.log(datas[4].message)

    // const emptyField = () => {
    //     document.getElementById('chatField').value= ''
    // }

    const handleNewMessageChange = (event) => {
        setNewMessage(event.target.value)
        
    }

    const handleSendMessage = function () {
        sendMessage(newMessage)
        setNewMessage('')
        document.getElementById('chatField').value= ''
    }

    return (<>
        <MDBContainer className="d-flex justify-content-center py-5">
            <ChatHeader>
               <MDBRow>
                    <MDBCol className='d-flex justify-content-center'>
                        <h2 className='p-3'>{`Your chat with ${user.firstname}`}</h2> 
                        <div className='w-m-50 mb-5 d-flex align-items-center'>   
                            <img //TODO: implement so it uses uploaded image by user
                                className="rounded-circle flex-shrink-0 mt-1"
                                alt="user face"
                                src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg"
                                data-holder-rendered="true"
                                height="50"
                            />
                            <img //TODO: implement so is only shown when user is online, will need function on backend to check if the user is
                                className="rounded-circle"
                                src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Ski_trail_rating_symbol-green_circle.svg/600px-Ski_trail_rating_symbol-green_circle.svg.png"
                                alt="user-status"
                                height="20"
                                style={{ transform: 'translate(-80%, 110%)' }}
                            />
                        </div> 
                    </MDBCol>                
                </MDBRow> 
            </ChatHeader>
            <MessagesArea>
               <MDBRow>
                    <div className="mb-4 d-flex flex-column justify-content-end">
                        {messages.map((message, i) => (
                            <div
                                key={i}
                                className={`w-m-50 mb-3 d-flex ${
                                    //TODO: implement so that this works correctly
                                    message.ownedByCurrentUser
                                        ? 'flex-row'
                                        : 'flex-row-reverse'
                                }`}                   
                            >                        
                                <Message
                                    style={{
                                    backgroundColor: `${
                                        message.ownedByCurrentUser
                                            ? '#7b64e7'
                                            : '#f4f4f4'
                                    }`,
                                    color: `${
                                        message.ownedByCurrentUser
                                            ? '#f4f4f4'
                                            : '#7b64e7'
                                    }`
                                }}>{message.message}</Message>
                            </div>
                        ))}
                    </div>
                </MDBRow> 
            </MessagesArea> 
            <WriteMessageArea>                       
                <MDBRow>                
                    <MDBCol className='d-flex align-items-center'>
                        <TextArea id="chatField"
                            value={newMessage}
                            onChange={handleNewMessageChange}
                            placeholder="Write message..."
                        />
                        <i onClick={handleSendMessage} className="far fa-paper-plane"></i>
                    </MDBCol>               
                </MDBRow> 
            </WriteMessageArea>            
        </MDBContainer>
        {/* This is quick hack to show the link in chat, TODO  */}
        <MDBContainer className='d-none d-md-block'>
            <Link to='/' className='p-4' style={{position: 'fixed', bottom: '80px'}}><span>Back to main page</span></Link>
        </MDBContainer>       
        </>
    )
}
