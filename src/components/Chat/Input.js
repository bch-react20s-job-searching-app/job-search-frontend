import { MDBInput, MDBContainer } from 'mdb-react-ui-kit'
import React from 'react'

export default function MessageInput({
    handleMessageInput,
    handleMessageSend,
    className,
}) {
    /**
     * Handles form submit on enter. Shift-enter can still be used for a newline input.
     * @param {*} e
     */
    const handleEnter = (e) => {
        // which = 13 means the enter key
        if (e.which === 13 && !e.shiftKey) {
            e.preventDefault()
            handleMessageSend()
            e.target.closest('form').reset()
        }
    }

    return (
        <MDBContainer>
            <form
                controlId="exampleForm.ControlTextarea1"
                onChange={handleMessageInput}
                onKeyPress={handleEnter}
                className="mb-0"
            >
                <MDBInput
                    as="textarea"
                    rows={2}
                    placeholder="Type a new message..."
                    style={{ resize: 'none' }}
                />
            </form>
        </MDBContainer>
    )
}
