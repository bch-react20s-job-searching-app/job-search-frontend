import React from 'react';
import { ThemeProvider } from 'styled-components';

const theme = {
  color: {
    bg: '#f4f4f4',
    lightShadow: '#ffffff',
    darkShadow: '#c3c3c3',
    text: '#333333',
    accent1: {
      main: '#e31c8f',
      light: '#eb60b1',
      dark: '#9f1464'
    },
    accent2: {
      main: '#4222dd',
      light: '#7b64e7',
      dark: '#2e189b'
    }
  },
  font: {
    style: {
      header: 'Quicksand',
      text: 'Lato'
    },
    size: {
      h1: '3.25rem',
      h2: '1.75rem',
      h3: '1.375rem',
      h4: '1.125rem',
      h5: '0.875rem',
      span: '1rem',
      text: '.9rem',
      inlineLink: '0.8rem',      
      label: '.9rem',
      smallButton: '1rem',
      largeButton: '1.2rem',
      icon: '2rem'
    }
  }
}

const Theme = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;