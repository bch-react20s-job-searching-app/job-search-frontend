import { createGlobalStyle } from 'styled-components'

const GlobalStyling = createGlobalStyle`

    * {
	    margin: 0;
	    padding: 0;
        color: ${({theme}) => theme.color.text};
        box-sizing: border-box;
    }
    *, *::after, *::before {
        box-sizing: inherit;
    }
    body {
	    position: relative;
        min-height: 100vh;
        background: ${({theme}) => theme.color.bg};
        padding-bottom: 4rem;
    } 
    h1 {        
        font-family: ${({theme}) => theme.font.style.header};
        font-weight: 500;
        font-size: ${({theme}) => theme.font.size.h1};
    }
    h2 {        
        font-family: ${({theme}) => theme.font.style.header};
        font-weight: 500;
        font-size: ${({theme}) => theme.font.size.h2};
    }
    h3 {        
        font-family: ${({theme}) => theme.font.style.header};
        font-weight: 400;
        font-size: ${({theme}) => theme.font.size.h3};
    }
    h4 {        
        font-family: ${({theme}) => theme.font.style.header};
        font-weight: 400;
        font-size: ${({theme}) => theme.font.size.h4};
    }  
    h5 {        
        font-family: ${({theme}) => theme.font.style.header};
        font-weight: 400;
        font-size: ${({theme}) => theme.font.size.h5};
    } 
    p   {
        font-family: ${({theme}) => theme.font.style.text};
        font-weight: 300;
        font-size: ${({theme}) => theme.font.size.text};
        margin-bottom: 0;
        padding: 0 .5rem;
    }   
    span {
        padding-bottom: .5rem;
        color: ${({theme}) => theme.color.accent1.main};
        font-family: ${({theme}) => theme.font.style.text};
        font-weight: bold;
        font-size: ${({theme}) => theme.font.size.span};
        padding: 1.2rem;
        transition: all 300ms;
        &:hover {
            font-size: 1.1rem;
            padding-left: .9rem;
        }
    }
    label {
        padding: 0 .5rem 0 0;
        color: ${({theme}) => theme.color.accent2.main};
        font-family: ${({theme}) => theme.font.style.text};
        font-weight: bold;
        font-size: ${({theme}) => theme.font.size.label};
    }
    i   {
        color: ${({theme}) => theme.color.accent1.main};
        font-size: ${({theme}) => theme.font.size.icon};
        margin: 0 .5rem;
    }
    a   {
        color: ${({theme}) => theme.color.accent1.main};
        font-family: ${({theme}) => theme.font.style.header};
        font-size: ${({theme}) => theme.font.size.h4};
    }


    @media only screen and (max-width: 576px) {
        h1 {
            font-size: 2.5rem;
        }
        h2 {
            font-size: 1.5rem;
        }
        h3 {
            font-size: 1.1rem;
        }
    }
`

export default GlobalStyling
