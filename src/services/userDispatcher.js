import axios from 'axios'
import * as actionTypes from '../store/user/userActions'

const signUpUrl = 'api/auth/register'
const signInUrl = 'api/auth/login'
const userRetrieveUrl = '/api/user/retrieve'
const updateUserUrl = '/api/user/update'
const signOutUrl = 'api/auth/signout'
const jobCreationUrl = 'api/job/add'
const googleAuthUrl = 'http://127.0.0.1:8080/api/auth/google'

export const initializeUserState = () => {
    return async (dispatch) => {
        let inState = await axios.post(userRetrieveUrl).then((resp) => {
            return resp.data
        })
        dispatch({
            type: actionTypes.SET_USER,
            //Returns either null, or user object
            payload: inState ? inState : null,
        })
    }
}

export const registerUser = (newUserData) => {
    return async (dispatch) => {
        let message
        const inState = await axios
            .post(signUpUrl, newUserData)
            .then((res) => {
                //ASSigning server response to message varribale
                message = res.data.message

                console.log(res.data)

                //Returns either null, or registered user object
                return res.data.user ? res.data.user : null
            })
            .catch((e) => console.log(e))
        dispatch({
            type: actionTypes.SET_USER,
            payload: inState,
        })
        return message
    }
}

export const signInUser = (user) => {
    return async (dispatch) => {
        let message
        let userOrUndef = await axios
            .post(signInUrl, user)
            .then((res) => {
                message = res.data.message

                //Returns either null, or logged in user object
                return res.data.user ? res.data.user : null
            })
            .catch((e) => console.log(e))
        dispatch({
            type: actionTypes.SET_USER,
            payload: userOrUndef,
        })
        return message
    }
}

export const signOutUser = () => {
    return async (dispatch) => {
        await axios.post(signOutUrl)
        dispatch({ type: actionTypes.SET_USER, payload: null })
    }
}

export const updateUser = (updatedUserData) => {
    return async (dispatch) => {
        //The best way to send images with React is to use this FormData class
        const form = new FormData()

        //We just iterate the whole updates object and for every entrie of the object we just appending it to the form
        for (let key in updatedUserData) {
            form.append(key, updatedUserData[key])
        }

        //Creating placeholder for message variable
        let message

        //Actually posting updates to the REST
        const updatedUser = await axios
            .post(updateUserUrl, form)
            .then((res) => {
                console.log(res)

                //Filling that message var
                message = res.data.message

                //Returns either null, or updated user object
                return res.data.user ? res.data.user : null
            })
            .catch((e) => console.log(e))
        dispatch({
            type: actionTypes.SET_USER,
            payload: updatedUser,
        })

        //Returning message, so it will be possible to print it to the user
        return message
    }
}

export const googleAuth = () => {
    return async (dispatch) => {
        window.open(googleAuthUrl, '_self')
    }
}

export const addJob = (jobObj) => {
    return async (dispatch) => {
        //The best way to send images with React is to use this FormData class
        const form = new FormData()

        //We just iterate the whole updates object and for every entrie of the object we just appending it to the form
        for (let key in jobObj) {
            form.append(key, jobObj[key])
        }

        //Creating placeholder for message variable
        let message

        //Actually posting updates to the REST
        const createdJob = await axios
            .post(jobCreationUrl, form)
            .then((res) => {
                console.log(res)

                //Filling that message var
                message = res.data.message

                //Returns either null, or created job object
                return res.data.job ? res.data.job : null
            })
            .catch((e) => console.log(e))

        //No dispatching here

        //Returning message, so it will be possible to print it to the user
        return message
    }
}
