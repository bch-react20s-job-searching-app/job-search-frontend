import { useEffect, useRef, useState } from 'react'
import socketIOClient from 'socket.io-client'
import axios from 'axios'

const SOCKET_SERVER_URL = process.env.SERVER_URL || 'http://localhost:8080'

const NEW_CHAT_MESSAGE_EVENT = 'newChatMessage' // Name of the event

export const useChat = (roomId) => {
    const [messages, setMessages] = useState([]) // Sent and received messages
    const socketRef = useRef()

    useEffect(() => {
        // loads message data from the DB and sets it to the messages constant
        const loadData = () => {
            return axios
                .get(`http://localhost:8080/api/messages`)
                .then((result) => {
                    setMessages(result.data)
                })
                .catch((error) => {
                    console.error('error: ', error)
                    //TODO: add proper error handling
                    this.setState({
                        // objects cannot be used as a react child
                        // -> <p>{error}</p> would throw otherwise
                        // data: `${setError}`,
                    })
                })
        }

        loadData()

        // Creates a WebSocket connection
        socketRef.current = socketIOClient(SOCKET_SERVER_URL, {
            query: { roomId },
        })

        // Listens for incoming messages and transmits it, the logic sending this to the database is carried out by the BACKEND
        socketRef.current.on(NEW_CHAT_MESSAGE_EVENT, (message) => {
            const incomingMessage = {
                ...message,
                ownedByCurrentUser: message.senderId === socketRef.current.id,
            }
            setMessages((messages) => [...messages, incomingMessage])
        })

        // Destroys the socket reference
        // when the connection is closed
        return () => {
            socketRef.current.disconnect()
        }
    }, [roomId])

    // Sends a message to the server that
    // forwards it to all users in the same room
    const sendMessage = (messageBody) => {
        socketRef.current.emit(NEW_CHAT_MESSAGE_EVENT, {
            message: messageBody,
            body: messageBody,
            senderId: socketRef.current.id,
        })
    }

    return { messages, sendMessage }
}
