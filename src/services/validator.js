/**
 *	Perfoms checking signUp object for errors, for example it checks email with regex and password to be at least 8 symbols
 * @param {object} formObject
 * @returns {array} error array
 */

const signUp = (formObject) => {
    const err = []

    if (!formObject.type) {
        err.push('type')
    }

    if (!formObject.email) {
        err.push('emailInput')
    } else {
        const testRes = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(
            formObject.email
        )
        if (!testRes) {
            err.push('emailInput')
        }
    }
    if (!formObject.password || formObject.password.length < 8) {
        err.push('passwordInput')
    }
    if (!formObject.username) {
        err.push('usernameInput')
    }
    return err
}

/**
 *	Perfoms checking signIn object for errors, for example it checks email with regex and password to be at least 8 symbols      @param {object} formObject
 * @returns {array} error array
 */

const signIn = (formObject) => {
    const err = []

    if (!formObject.email) {
        err.push('emailInput')
    } else {
        const testRes = /^[^\s@]+@[^\s@]+$/.test(formObject.email)
        if (!testRes) {
            err.push('emailInput')
        }
    }
    if (!formObject.password || formObject.password.length < 8) {
        err.push('passwordInput')
    }
    return err
}

/**
 * Perfoms checking job creation object for errors
 * @param {object} jobObj
 * @returns {array} error array
 */
const createJob = (jobObj) => {
    const err = []

    if (!jobObj.title) {
        err.push('title')
    }
    if (!jobObj.seniority) {
        err.push('seniority')
    }
    if (!jobObj.description) {
        err.push('description')
    }
    if (!jobObj.requiredSkills) {
        err.push('requiredSkills')
    }
    if (!jobObj.desirableSkills) {
        err.push('desirableSkills')
    }
    if (!jobObj.location) {
        err.push('location')
    }
    if (!jobObj.photo) {
        err.push('photo')
    }
    if (!jobObj.workingLanguage) {
        err.push('workingLanguage')
    }
    if (!jobObj.companyWebsite) {
        err.push('companyWebsite')
    }
    if (!jobObj.contractType) {
        err.push('contractType')
    }

    return err
}

export { signIn, signUp, createJob }
