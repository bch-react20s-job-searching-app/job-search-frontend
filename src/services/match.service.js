import axios from 'axios'

///
////// EMPLOYEE METHODS
//

export const retrieveAllJobs = (setAllJobs) => {
    axios
        .get(`http://localhost:8080/api/match/jobs`)
        .then((response) => {
            setAllJobs(response.data)
        })
        .catch((error) => {
            console.log(error)
        })
}
export const relevantJobs = (setJobs, setLoading, userId) => {
    axios
        .get(`http://localhost:8080/api/match/relevant-jobs/${userId}`)
        .then((response) => {
            setJobs(response.data)
            console.log(response.data)
            setLoading(false)
        })
        .catch((error) => {
            console.log(error)
        })
}

export const likeJob = (jobId, userId) => {
    console.log('jobId:', jobId)
    axios
        .put(`http://localhost:8080/api/match/likejob/${userId}`, {
            job: jobId,
        })

        .then((response) => {})
        .catch((error) => {
            console.log(error)
        })
}

///
////// EMPLOYER METHODS
//

export const retrieveAllEmployees = (setAllEmployees) => {
    axios
        .get(`http://localhost:8080/api/match/employees`)
        .then((response) => {
            setAllEmployees(response.data)
        })
        .catch((error) => {
            console.log(error)
        })
}

export const relevantEmployees = (setEmployees, setLoading, selectedJob) => {
    axios
        .get(
            `http://localhost:8080/api/match/relevant-employees/${selectedJob}`
        )
        .then((response) => {
            setEmployees(response.data)
            console.log(setEmployees)
            console.log(response.data)
            setLoading(false)
        })
        .catch((error) => {
            console.log(error)
        })
}

export const likeEmployee = (employeeId, jobId) => {
    console.log('EmployeeID', employeeId)
    axios
        .put(`http://localhost:8080/api/match/likejob/${jobId}`, {
            user: employeeId,
        })

        .then((response) => {})
        .catch((error) => {
            console.log(error)
        })
}
