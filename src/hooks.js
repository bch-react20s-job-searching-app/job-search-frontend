// This file is for custom hooks
import { useState, useEffect } from 'react'

// Hook for closing element if clicked anywhere expect the element itself (used in mobile menu)
export const ClickOutside = (ref, handler) => {
  useEffect(() => {
    const listener = event => {
      // do nothing, if clicked on element
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      handler(event);
    };
    document.addEventListener('mousedown', listener);
    return () => {
      document.removeEventListener('mousedown', listener);
    };
  },
  [ref, handler]
  );
};

// setting breakpoint based conditional rendering 
export const useViewportWidth = () => {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth)
      
      useEffect(() => {
        const handleWindowResize = () => setWindowWidth(window.innerWidth)
        window.addEventListener('resize', handleWindowResize)
        return () => window.removeEventListener('resize', handleWindowResize);
    }, [])
    return { windowWidth }
}
